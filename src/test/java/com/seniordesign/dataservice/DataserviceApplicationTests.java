package com.seniordesign.dataservice;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DataserviceApplicationTests 
{
    @Autowired
    private MockMvc mvc;
    
    @Test
    public void test1() throws Exception
    {
    	System.out.println("--- Test 1 [ get hello world ]---");
    	this.mvc.perform(get("/api/hello")).andExpect(status().isOk())
				.andExpect(content().string("Hello World!"));
    }
    
    @Test
    public void test2() throws Exception
    {
    	System.out.println("--- Test 2 [ test update hash ]---");
    	this.mvc.perform(get("/api/updateHash")).andExpect(status().isOk());
    }
    
    @Test
    public void test3() throws Exception
    {
        System.out.println("--- Test 3 [ test post and get ]---");
    }
}
