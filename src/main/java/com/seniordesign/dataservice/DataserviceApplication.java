package com.seniordesign.dataservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataserviceApplication
{
    private static final Logger logger = LoggerFactory.getLogger(DataserviceApplication.class);

    @Autowired
    private YMLConfiguration config; //Dependency injection to read application.yml

    public static void main(String[] args)
    {
        SpringApplication.run(DataserviceApplication.class, args);
    }
}
