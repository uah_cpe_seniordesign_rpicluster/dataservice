package com.seniordesign.dataservice;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

//Bean to contain data from the worker nodes.
public class HostData
{
    //Filled out with the service:

    private String uuid;
    private Date recTime;

    //Specified by the clients:
    private String hostname;
    private double totalCpuUtilization;
    private List<Double> perCoreCpuUtilization;
    private double physicalMemoryTotal;
    private double physicalMemoryUsed;
    private double virtualMemoryTotal;
    private double virtualMemoryUsed;
    private double cpuTemperature;
    private double rxBytes;
    private double txBytes;
    private int txPackets;
    private int rxPackets;
    private double ambientTemp;
    private double moduleCurrentDraw;
    private double backplaneVoltage;
    private double processorVoltage;

    public HostData() {  }

    public String getHostname() { return hostname; }
    public void setHostname(String hostname) { this.hostname = hostname; }

    public List<Double> getPerCoreCpuUtilization() { return this.perCoreCpuUtilization; }
    public void setPerCoreCpuUtilization(List<Double> u) { this.perCoreCpuUtilization = u; }

    public double getPhysicalMemoryTotal() { return this.physicalMemoryTotal; }
    public void setPhysicalMemoryTotal(double p) { this.physicalMemoryTotal = p; }

    public double getTotalCpuUtilization() { return this.totalCpuUtilization; }
    public void setTotalCpuUtilization(double cpuUtilization) { this.totalCpuUtilization = cpuUtilization; }

    public double getPhysicalMemoryUsed() { return this.physicalMemoryUsed; }
    public void setPhysicalMemoryUsed(double p) { this.physicalMemoryUsed = p; }

    public double getVirtualMemoryTotal() {
        return virtualMemoryTotal;
    }
    public void setVirtualMemoryTotal(double virtualMemoryTotal) {
        this.virtualMemoryTotal = virtualMemoryTotal;
    }

    public double getVirtualMemoryUsed() {
        return virtualMemoryUsed;
    }
    public void setVirtualMemoryUsed(double virtualMemoryUsed) {
        this.virtualMemoryUsed = virtualMemoryUsed;
    }

    public double getCpuTemperature() { return cpuTemperature; }
    public void setCpuTemperature(double temperature) { this.cpuTemperature = temperature; }

    public int getTxPackets() { return this.txPackets; }
    public void setTxPackets(int txPackets) { this.txPackets = txPackets; }

    public int getRxPackets() { return this.rxPackets; }
    public void setRxPackets(int rxPackets) { this.rxPackets = rxPackets; }

    public double getRxBytes() { return rxBytes; }
    public void setRxBytes(double rxBytes) { this.rxBytes = rxBytes; }

    public double getTxBytes() { return txBytes; }
    public void setTxBytes(double txBytes) { this.txBytes = txBytes; }

    public double getAmbientTemp() {
        return ambientTemp;
    }

    public void setAmbientTemp(double ambientTemp) {
        this.ambientTemp = ambientTemp;
    }

    public double getModuleCurrentDraw() {
        return moduleCurrentDraw;
    }

    public void setModuleCurrentDraw(double moduleCurrentDraw) {
        this.moduleCurrentDraw = moduleCurrentDraw;
    }

    public double getBackplaneVoltage() {
        return backplaneVoltage;
    }

    public void setBackplaneVoltage(double backplaneVoltage) {
        this.backplaneVoltage = backplaneVoltage;
    }

    public double getProcessorVoltage() {
        return processorVoltage;
    }

    public void setProcessorVoltage(double processorVoltage) {
        this.processorVoltage = processorVoltage;
    }

    // The data below is set by the rest controller on receive.
    public String getUuid() { return this.uuid; }
    public void setUuid(String Uuid) { this.uuid = Uuid; }

    public Date getRecTime() { return this.recTime; }
    public void setRecTime(Date recTime) { this.recTime = recTime; }

    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();
        sb.append("\n\tUUID: " + this.uuid + "\n");
        sb.append("\tPacket Receive Time: " + this.recTime.toString() + "\n");
        sb.append("\tClient Hostname: " + this.hostname + "\n");
        sb.append("\tTotal CPU Utilization: " + this.totalCpuUtilization + "\n");
        sb.append("\tCPU Utilization (per core): " + Arrays.toString(this.perCoreCpuUtilization.toArray()) + "\n");
        sb.append("\tPhysical Memory Total: " + this.physicalMemoryTotal + "\n");
        sb.append("\tPhysical Memory Used: " + this.physicalMemoryUsed + "\n");
        sb.append("\tVirtual Memory Total: " + this.virtualMemoryTotal + "\n");
        sb.append("\tVirtual Memory Used: " + this.virtualMemoryUsed + "\n");
        sb.append("\tTx Bytes (kB/s): " + this.txBytes + "\n");
        sb.append("\tRx Bytes (kB/s): " + this.rxBytes + "\n");
        sb.append("\tTx Packets: " + this.txPackets + "\n");
        sb.append("\tRx Packets: " + this.rxPackets + "\n");
        sb.append("\tCPU Temperature (C): " + this.cpuTemperature + "\n");
        return sb.toString();
    }
}
