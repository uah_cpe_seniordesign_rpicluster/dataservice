package com.seniordesign.dataservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.security.MessageDigest;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class ApplicationController
{
    private static final Logger logger = LoggerFactory.getLogger(ApplicationController.class);

    private final Map<String, HostData> dataMap;

    public ApplicationController()
    {
        dataMap = new ConcurrentHashMap<>(); // Thread safe map
        logger.info("**************************************************");
        logger.info("************ Data Service Initialized ************");
        logger.info("**************************************************");
        logger.debug("Available Request Mappings:");
        logger.debug("\t/api/hello          - Returns 'Hello World!'");
        logger.debug("\t/api/addHostData    - Expects a POST of a data object.");
        logger.debug("\t/api/getHostData    - Get data for a hostname.");
        logger.debug("\t/api/getAllHostData - Get host data map object.");
        logger.debug("\t/api/getHostsList   - Get a string array of hostnames.");
        logger.debug("\t/api/updateHash     - Get MD5 sum of all UUID objects.\n\n");
    }

    @RequestMapping("/hello")
    public String getHello()
    {
        return "Hello World!";
    }

    @RequestMapping(value = "/addHostData", method = RequestMethod.POST)
    public void addHostData(HttpServletRequest req, @RequestBody HostData data)
    {

        UUID uuid = UUID.randomUUID();
        data.setUuid(uuid.toString());
        Date d = new Date();
        data.setRecTime(d);

        logger.debug("Received Data from: {}", data.getHostname());
        logger.trace("Host data received: {}", data.toString());

        String remoteAddr = data.getHostname();

        if (!dataMap.containsKey(remoteAddr))
        {
            //Assume new host added to the cluster.
            dataMap.put(remoteAddr, data);
        }
        else
        {
            //Assume the host has already been added.
            dataMap.remove(remoteAddr); //Remove the old entry.
            dataMap.put(remoteAddr, data); //Add new data.
        }

    }
    //Request mapping for host specific data.
    @RequestMapping(value = "/getHostData")
    public HostData getHostData(HttpServletRequest req, @RequestParam(value = "host") String host)
    {
        if (!dataMap.containsKey(host))
        {
            logger.warn("Host {} requested a data object that doesn't exist: {}",req.getRemoteAddr(),host);
            return null;
        }
        return this.dataMap.get(host);
    }
    //Get all host data by host by returning the map bean.
    @RequestMapping(value = "/getAllHostData")
    public Map<String,HostData> getAllHostData()
    {
        return this.dataMap;
    }

    @RequestMapping(value = "/getHostsList")
    public List<String> getHostsList()
    {
        return new ArrayList<>(this.dataMap.keySet());
    }

    @RequestMapping(value = "/updateHash")
    public String getUpdateHash()
    {
        String result = "null"; //
        Collection<HostData> tempData = this.dataMap.values();
        List<String> tempUuids = new ArrayList<>();

        for (HostData h : tempData)
            tempUuids.add(h.getUuid());

        ByteArrayOutputStream baos = null;
        try
        {
            MessageDigest md = MessageDigest.getInstance("MD5");
            baos = new ByteArrayOutputStream();

            for (String uuid : tempUuids)
                baos.write(uuid.getBytes());

            byte[] oof = md.digest(baos.toByteArray());
            BigInteger bigmn = new BigInteger(1,oof);
            result = bigmn.toString(16); //hex
        }
        catch (NoSuchAlgorithmException nsae) //get the algo exception
        {
            nsae.printStackTrace();
            logger.error("Algorithm not supported! Exception context: '{}', with cause '{}'.",
                    nsae.getMessage(), nsae.getCause());
        }
        catch (Exception ex) //swallow all other exceptions
        {
            ex.printStackTrace();
            logger.error("Unhandled exception: '{}'.", ex.getMessage());
        }
        finally
        {
            if (baos != null)
                try { baos.close(); } catch (Exception ex) {  } //Swallow the exception
        }
        return result; //will return "null" if there is a prob.
    }
}
