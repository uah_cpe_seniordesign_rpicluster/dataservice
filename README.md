# DataService - RESTful service for RPi cluster

### Pre Build dependencies:

- maven
- openjdk-8-jdk
- git

### Building the project

- Clone the project
- Change directory to the project root and run `mvn package`
- On completion, a new directory with an executable `.jar` will be created called `target/`
- Run the service via `java -jar <jar-name>.jar`

- Note: If the default port 8081 needs to be changed, specify it in a `application.yml` file in the same directory as the jar:

Like this:

```
server:
  port: 8081
```

- Logging levels can also be adjusted as follows:

```
logging:
  level:
    ROOT: DEBUG #Sets the logging level for all packages
```

```
logging:
  level:
     com.seniordesign: TRACE #Sets logging level for our code.
```